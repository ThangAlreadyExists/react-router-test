import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './components/Home';
import Product from './components/Product';
import About from './components/About';
import Header from './components/Header';

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Route path="/" exact component={Home} />
        <Route path="/product" exact component={Product} />
        <Route path="/about" exact component={About} />
      </div>
    </Router>
  );
}

export default App;
