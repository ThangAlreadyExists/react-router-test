import React, { Component } from 'react';
import { Link } from "react-router-dom";
class Header extends Component {
  render() {
    return (
      <div>
        <nav style={{float: "left", width: "20%", backgroundColor: "#ccc"}}>
          <div>
            <Link to="/">Home</Link>
          </div>
          <div>
            <Link to="/product">Product</Link>
          </div>
          <div>
            <Link to="/about">About</Link>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;