import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Product extends Component {
  render() {
    const { match } = this.props;
    return (
      <div>
        <h2>Product</h2>
        <Router>
          <nav style={{ float: "left", width: "20%", backgroundColor: "#ccc" }}>
            <div>
              <Link to={`${match.url}/`}>All</Link>
            </div>
            <div>
              <Link to={`${match.url}/phone`}>Phone</Link>
            </div>
            <div>
              <Link to={`${match.url}/laptop`}>Laptop</Link>
            </div>
          </nav>

          <Route
            exact
            path={match.path}
            render={() => <h3>All.</h3>}
          />
          <Route
            exact
            path={match.path + '/phone'}
            render={() => <h3>Phone.</h3>}
          />
          <Route
            exact
            path={match.path + '/laptop'}
            render={() => <h3>Lap.</h3>}
          />
        </Router>
      </div>
    );
  }
}

export default Product;